import axios from 'axios';

const KEY = 'AIzaSyALXhl5q0tFuGQNMpg4PUWn4ADbSueER0I';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        type: 'video',
        maxResults: 5,
        key: KEY
    }
});